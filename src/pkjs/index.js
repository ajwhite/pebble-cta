var message_keys = require('message_keys');

// ================
// Clay
// ================
var Clay = require('pebble-clay');
var clayConfig = require('./clay-config.json');
var clay = new Clay(clayConfig, null, { autoHandleEvents: false });

function clear_clay_data()
{
    clay.setSettings("favorite_name", "");
    clay.setSettings("favorite_id", "");
    clay.setSettings("clear_favorites", 0);
}

Pebble.addEventListener('showConfiguration', function(e) {
    Pebble.openURL(clay.generateUrl());
});

Pebble.addEventListener('webviewclosed', function(e) {
    if (e && !e.response) {
        return;
    }

    // Get the keys and values from each config item
    var config = clay.getSettings(e.response);

    // Don't send favorite config to watch - store it here on the phone
    if (config[message_keys.favorite_id]) {
        var new_favorite = JSON.parse(config[message_keys.favorite_id]);

        // Use custom favorite name
        if (config[message_keys.favorite_name])
            new_favorite['name'] = config[message_keys.favorite_name];

        var favorites = load_favorites();

        favorites.push(new_favorite);

        save_favorites(favorites);

        console.log('Favorites: ' + JSON.stringify(favorites));
    }

    if (config[message_keys.clear_favorites]) {
        console.log('Clearing favorites');
        save_favorites([]);
    }

    // Send settings values to watch side
    // Pebble.sendAppMessage(config, function(e) {
    //     console.log('Sent config data to Pebble');
    // }, function(e) {
    //     console.log('Failed to send config data!');
    //     console.log(JSON.stringify(e));
    // });
});


// ================
// Favorites
// ================
function load_favorites()
{
    var favorites = localStorage.getItem('favorites');
    console.log('Loaded favorites: ' + JSON.stringify(favorites));
    if (favorites)
        return JSON.parse(favorites);
    else
        return null;
}

function save_favorites(favorites)
{
    localStorage.setItem('favorites', JSON.stringify(favorites));
    console.log('Saved favorites: ' + JSON.stringify(favorites));
}



// ================
// CTA
// ================
var api_endpoint = 'http://lapi.transitchicago.com/api/1.0/ttarrivals.aspx';
var api_key = 'b6150362dd184d1cbf15c35755d7b0ad';

var train_line_map = {
    'Red' : 0,
    'Blue': 1,
    'Brn' : 2,
    'G'   : 3,
    'Org' : 4,
    'Pink': 5,
    'P'   : 6,
    'Y'   : 7
};

function send_next_train(trains, index)
{
    // Calculate due time (in seconds)
    var due_time = new Date(trains[index].arrT);
    var due_time_secs = (due_time.getTime() / 1000);
    var now_secs = Math.floor(Date.now() / 1000);
    var time_diff_mins = Math.floor((due_time_secs - now_secs) / 60);

    var train = {
        'terminus'    : trains[index].destNm,
        'line'        : train_line_map[trains[index].rt],
        'due_time'    : time_diff_mins
    };

    Pebble.sendAppMessage(train, function () {
        index++;

        if (index < trains.length) {
            send_next_train(trains, index);
        } else {
            Pebble.sendAppMessage({'all_trains_sent': 1});
        }
    }, function() {
        console.log('Item transmission failed at index: ' + index);
    });
}

function send_trains(trains)
{
    Pebble.sendAppMessage({'train_count': trains.length}, function () {
        send_next_train(trains, 0);
    }, function () {
        console.log('Failed to send train count');
    });
}

function handle_api_response()
{
    if (this.status === 200) {
        var json = JSON.parse(this.responseText);

        if (!json.ctatt.hasOwnProperty('eta')) {
            Pebble.sendAppMessage({'train_count': 0});
            return;
        }

        // Sort by arrival time (soonest first)
        var sorted_json = json.ctatt.eta.sort(function (a, b) {
            var a_time = Date.parse(a.arrT);
            var b_time = Date.parse(b.arrT);

            if (a_time > b_time) return 1;
            if (a_time < b_time) return -1;
            return 0;
        });
        send_trains(sorted_json);
    }
}

function fetch_station_arrivals(station)
{
    var req = new XMLHttpRequest();

    req.open('GET',
             api_endpoint +
             '?outputType=JSON' +
             '&key=' + api_key +
             '&mapid=' + station
            );

    req.onload = handle_api_response;

    req.send();
}

function send_next_favorite(favorites, index)
{
    var favorite = {
        'favorite_name' : favorites[index].name,
        'favorite_id'   : favorites[index].station_id
    };

    console.log("Sending favorite: " + JSON.stringify(favorite));

    Pebble.sendAppMessage(favorite, function () {
        index++;

        if (index < favorites.length) {
            send_next_favorite(favorites, index);
        } else {
            Pebble.sendAppMessage({'all_favorites_sent': 1});
        }
    }, function() {
        console.log('Item transmission failed at index: ' + index);
    });
}

function send_favorites(favorites)
{
    Pebble.sendAppMessage({'favorite_count': favorites.length}, function () {
        send_next_favorite(favorites, 0);
    }, function () {
        console.log('Failed to send favorites count');
    });
}

Pebble.addEventListener('ready', function() {
    clear_clay_data();

    if (!load_favorites())
        save_favorites([]);

    console.log('PebbleKit JS ready.');
    Pebble.sendAppMessage({'js_ready': 1});
});

Pebble.addEventListener('appmessage', function (e) {
    var dict = e.payload;

    if (dict.hasOwnProperty('favorite_count')) {
        var favorites = load_favorites();
        console.log('Sending favorites');
        send_favorites(favorites);
    } else if (dict.hasOwnProperty('station')) {
        console.log('Fetching info for station ' + dict['station']);
        fetch_station_arrivals(dict['station']);
    }
});
