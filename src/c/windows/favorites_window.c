#include <pebble.h>

#include "lib/cta_lines.h"
#include "lib/favorites.h"
#include "lib/comm.h"
#include "windows/favorites_window.h"
#include "windows/cta_tracker_window.h"
#include "external/dialog_config_window.h"

TextLayer *s_text_layer;
MenuLayer *s_menu_layer;

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *callback_context)
{
    FavoritesList *favorites_list = callback_context;

    return favorites_list->favorites_count;
}

static void menu_draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context)
{
    FavoritesList *favorites_list = callback_context;
    GRect bounds = layer_get_bounds(cell_layer);

    graphics_draw_text(ctx,
                       favorites_list->favorites[cell_index->row].name,
                       fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD),
                       GRect(10, 5, bounds.size.w - 10, bounds.size.h - 5),
                       GTextOverflowModeTrailingEllipsis,
                       PBL_IF_ROUND_ELSE(GTextAlignmentCenter, GTextAlignmentLeft),
                       NULL);
}

static void menu_select_click_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *context)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    FavoritesList *favorites_list = context;

    int station_id = favorites_list->favorites[cell_index->row].station_id;

    cta_tracker_window_push_station(station_id);
}

static void favorites_window_load(Window *window)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    FavoritesList *favorites_list = window_get_user_data(window);

    Layer *root_layer    = window_get_root_layer(window);
    GRect menu_bounds    = layer_get_bounds(root_layer);

    window_set_background_color(window, GColorVividCerulean);

#ifndef PBL_ROUND
    GRect text_bounds    = menu_bounds;

    text_bounds.size.h = 35;
    s_text_layer = text_layer_create(text_bounds);

    text_layer_set_font(s_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
    text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
    text_layer_set_background_color(s_text_layer, GColorVividCerulean);
    text_layer_set_text_color(s_text_layer, GColorBlack);
    text_layer_set_text(s_text_layer, "Favorites");

    layer_add_child(root_layer, text_layer_get_layer(s_text_layer));

    menu_bounds.origin.y += text_bounds.size.h;
    menu_bounds.size.h   -= text_bounds.size.h;
#endif

    s_menu_layer = menu_layer_create(menu_bounds);

    menu_layer_set_callbacks(s_menu_layer, favorites_list, (MenuLayerCallbacks) {
            .get_num_rows = menu_get_num_rows_callback,
            .draw_row     = menu_draw_row_callback,
            .select_click = menu_select_click_callback,
        });

    menu_layer_set_click_config_onto_window(s_menu_layer, window);

    menu_layer_set_normal_colors(s_menu_layer, GColorVividCerulean, GColorBlack);
    menu_layer_set_highlight_colors(s_menu_layer, GColorWhite, GColorVividCerulean);

    layer_add_child(root_layer, menu_layer_get_layer(s_menu_layer));
}

static void favorites_window_unload(Window *window)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    FavoritesList *favorites_list = window_get_user_data(window);

#ifndef PBL_ROUND
    text_layer_destroy(s_text_layer);
#endif

    menu_layer_destroy(s_menu_layer);
    favorites_list_destroy(favorites_list);
    window_destroy(window);
}

static void __favorites_window_push(FavoritesList *favorites_list)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    Window *favorites_window = window_create();
    WindowHandlers favorites_window_handlers = {
        .load   = favorites_window_load,
        .unload = favorites_window_unload
    };

    window_set_window_handlers(favorites_window, favorites_window_handlers);

    window_set_user_data(favorites_window, favorites_list);

    window_stack_push(favorites_window, true);
}

static void favorites_transaction_callback(CtaCommTransaction *transaction)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    FavoritesList *favorites_list = transaction->transaction_data.favorites_transaction.favorites_list;

    free(transaction);

    if (favorites_list == NULL) /* No favorites available */
        dialog_config_window_push("No favorites yet!");
    else
        __favorites_window_push(favorites_list);
}

void favorites_window_push(void)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    CtaCommTransaction *transaction = calloc(1, sizeof(CtaCommTransaction));

    transaction->transaction_type = CTA_FAVORITES_TRANSACTION;
    transaction->callback = favorites_transaction_callback;

    comm_perform_transaction(transaction);
}
