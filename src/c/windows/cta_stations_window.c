#include <pebble.h>

#include "lib/cta_lines.h"
#include "windows/cta_tracker_window.h"

static TextLayer *s_text_layer;
static MenuLayer *s_menu_layer;

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *callback_context)
{
    CtaLine *current_line = callback_context;

    return current_line->station_count;
}

static void menu_draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context)
{
    CtaLine *current_line = callback_context;
    GRect bounds = layer_get_bounds(cell_layer);

    graphics_draw_text(ctx,
                       current_line->stations[cell_index->row].name,
                       fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD),
                       GRect(10, 5, bounds.size.w - 10, bounds.size.h - 5),
                       GTextOverflowModeTrailingEllipsis,
                       PBL_IF_ROUND_ELSE(GTextAlignmentCenter, GTextAlignmentLeft),
                       NULL);
}

static void menu_select_click_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context)
{
    CtaLine *current_line = callback_context;

    int station_id = current_line->stations[cell_index->row].station_id;

    cta_tracker_window_push_station(station_id);
}

static void cta_stations_window_load(Window *window)
{
    CtaLine *current_line = window_get_user_data(window);

    Layer *root_layer = window_get_root_layer(window);
    GRect menu_bounds = layer_get_bounds(root_layer);

    GColor primary_color   = cta_get_primary_color(current_line->line_id);
    GColor secondary_color = cta_get_secondary_color(current_line->line_id);

    window_set_background_color(window, primary_color);

#ifndef PBL_ROUND
    GRect text_bounds    = menu_bounds;

    text_bounds.size.h = 35;
    s_text_layer = text_layer_create(text_bounds);

    text_layer_set_font(s_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
    text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
    text_layer_set_background_color(s_text_layer, primary_color);
    text_layer_set_text_color(s_text_layer, secondary_color);
    text_layer_set_text(s_text_layer, current_line->name);

    layer_add_child(root_layer, text_layer_get_layer(s_text_layer));

    menu_bounds.origin.y += text_bounds.size.h;
    menu_bounds.size.h   -= text_bounds.size.h;
#endif

    s_menu_layer = menu_layer_create(menu_bounds);

    menu_layer_set_callbacks(s_menu_layer, current_line, (MenuLayerCallbacks) {
            .get_num_rows = menu_get_num_rows_callback,
            .draw_row     = menu_draw_row_callback,
            .select_click = menu_select_click_callback,
        });

    menu_layer_set_click_config_onto_window(s_menu_layer, window);

    menu_layer_set_normal_colors(s_menu_layer, primary_color, secondary_color);
    menu_layer_set_highlight_colors(s_menu_layer, secondary_color, primary_color);

    layer_add_child(root_layer, menu_layer_get_layer(s_menu_layer));
}

static void cta_stations_window_unload(Window *window)
{
#ifndef PBL_ROUND
    text_layer_destroy(s_text_layer);
#endif

    menu_layer_destroy(s_menu_layer);
    window_destroy(window);
}

void cta_stations_window_push(CtaLineID line_id)
{
    Window *cta_stations_window = window_create();
    WindowHandlers cta_stations_window_handlers = {
        .load   = cta_stations_window_load,
        .unload = cta_stations_window_unload
    };

    window_set_window_handlers(cta_stations_window, cta_stations_window_handlers);

    window_set_user_data(cta_stations_window, cta_stations_get_line(line_id));

    window_stack_push(cta_stations_window, true);
}
