#ifdef PBL_ROUND
#include <pebble.h>
#include "lib/color_menu_layer.h"
#include "lib/cta_lines.h"
#include "windows/color_menu_layer_window.h"
#include "windows/cta_l_trains_window.h"
#include "windows/cta_stations_window.h"

static void cta_l_trains_callback(int index, void *context);

static ColorMenuItem menu_items[] = {
    {
        .title = "Red Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Blue Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Brown Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Green Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Orange Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Pink Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Purple Line",
        .callback = cta_l_trains_callback,
    },
    {
        .title = "Yellow Line",
        .callback = cta_l_trains_callback,
    },
};

static void cta_l_trains_callback(int index, void *context)
{
    cta_stations_window_push((CtaLineID)index);
}

static void cta_l_trains_colors_init()
{
    for (unsigned int i = 0; i < CTA_yellow_line || i < (sizeof(menu_items) / sizeof(menu_items[0])); i++) {
        menu_items[i].primary_color = cta_get_primary_color(i);
        menu_items[i].secondary_color = cta_get_secondary_color(i);
    }
}

void cta_l_trains_window_push(void)
{
    cta_l_trains_colors_init();

    color_menu_layer_window_push(menu_items, sizeof(menu_items)/sizeof(menu_items[0]), NULL, NULL);
}
#endif
