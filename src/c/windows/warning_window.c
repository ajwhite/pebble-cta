#include <pebble.h>

#include "windows/no_trains_window.h"

static TextLayer *text_layer;
static Layer     *canvas_layer;
static char      *no_trains_string;
static GDrawCommandImage *draw_command_image;

static void update_proc(Layer *layer, GContext *ctx) {
    GSize img_size = gdraw_command_image_get_bounds_size(draw_command_image);
    GRect bounds = layer_get_bounds(layer);

    // Place image in the center of the Window
    const GEdgeInsets frame_insets = {
        .top = (bounds.size.h - img_size.h) / 2,
        .left = (bounds.size.w - img_size.w) / 2
    };

    if (draw_command_image)
        gdraw_command_image_draw(ctx, draw_command_image, grect_inset(bounds, frame_insets).origin);
}

static void no_trains_window_load(Window *window)
{
    Layer *root_layer = window_get_root_layer(window);
    GRect root_bounds = layer_get_bounds(root_layer);

    GRect canvas_bounds = root_bounds;
    canvas_bounds.size.h -= PBL_IF_ROUND_ELSE(90, 70);

    GRect text_layer_bounds = root_bounds;
    text_layer_bounds.origin.y += canvas_bounds.size.h;
    text_layer_bounds.size.h   -= canvas_bounds.size.h;

    draw_command_image = gdraw_command_image_create_with_resource(RESOURCE_ID_NO_TRAINS_ICON);

    canvas_layer = layer_create(canvas_bounds);
    text_layer   = text_layer_create(text_layer_bounds);

    text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
    text_layer_set_font(text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
    text_layer_set_background_color(text_layer, GColorClear);
    text_layer_set_text(text_layer, no_trains_string);

    layer_set_update_proc(canvas_layer, update_proc);

    layer_add_child(root_layer, canvas_layer);
    layer_add_child(root_layer, text_layer_get_layer(text_layer));
}

static void no_trains_window_unload(Window *window)
{
    text_layer_destroy(text_layer);
    layer_destroy(canvas_layer);
    gdraw_command_image_destroy(draw_command_image);
    window_destroy(window);
}

void no_trains_window_push(char *string)
{
    Window *no_trains_window = window_create();
    WindowHandlers no_trains_window_handlers = {
        .load   = no_trains_window_load,
        .unload = no_trains_window_unload,
    };

    window_set_window_handlers(no_trains_window, no_trains_window_handlers);

    no_trains_string = string;

    window_set_background_color(no_trains_window, GColorYellow);

    window_stack_push(no_trains_window, true);
}
