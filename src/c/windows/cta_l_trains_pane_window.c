#ifdef PBL_RECT
#include <pebble.h>

#include "lib/cta_lines.h"
#include "windows/cta_l_trains_window.h"
#include "windows/cta_stations_window.h"

#define CTA_L_TRAINS_WINDOW_BACKGROUND_COLOR GColorVividCerulean

typedef void (*CtaLTrainsWindowSelectCallback)(uint16_t current_index);

typedef struct CtaLTrainsWindowItem {
    char *title;
    uint32_t resource_id;
    CtaLTrainsWindowSelectCallback callback;

    GDrawCommandImage *prv_image;
    Layer *prv_item_layer;
    Layer *prv_canvas_layer;
    TextLayer *prv_text_layer;
    int16_t prv_offset;
} CtaLTrainsWindowItem;


static Window *cta_l_trains_window;
static ScrollLayer *cta_l_trains_scroll_layer;
static ContentIndicator *cta_l_trains_content_indicator;
static Layer *up_content_indicator;
static Layer *down_content_indicator;

static int16_t pane_offset;
static int16_t max_offset;

static void cta_l_trains_select_callback(uint16_t current_index);

CtaLTrainsWindowItem cta_l_trains_window_items[] = {
    {
        .title = "Red",
        .resource_id = RESOURCE_ID_RED_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Blue",
        .resource_id = RESOURCE_ID_BLUE_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Brown",
        .resource_id = RESOURCE_ID_BROWN_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Green",
        .resource_id = RESOURCE_ID_GREEN_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Orange",
        .resource_id = RESOURCE_ID_ORANGE_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Pink",
        .resource_id = RESOURCE_ID_PINK_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Purple",
        .resource_id = RESOURCE_ID_PURPLE_LINE,
        .callback = cta_l_trains_select_callback,
    },
    {
        .title = "Yellow",
        .resource_id = RESOURCE_ID_YELLOW_LINE,
        .callback = cta_l_trains_select_callback,
    },
};

#define NUM_CTA_L_TRAINS_WINDOW_ITEMS (sizeof(cta_l_trains_window_items)/sizeof(cta_l_trains_window_items[0]))

static void cta_l_trains_window_items_update_proc(Layer *layer, GContext *ctx) {
    CtaLTrainsWindowItem *cta_l_trains_window_item = *(CtaLTrainsWindowItem**)layer_get_data(layer);

    GSize img_size = gdraw_command_image_get_bounds_size(cta_l_trains_window_item->prv_image);
    GRect bounds = layer_get_bounds(layer);

    // Place image in the center of the Window
    const GEdgeInsets frame_insets = {
        .top = (bounds.size.h - img_size.h) / 2 - 5,
        .left = (bounds.size.w - img_size.w) / 2 - 5
    };

    gdraw_command_image_draw(ctx, cta_l_trains_window_item->prv_image, grect_inset(bounds, frame_insets).origin);
}

static inline uint16_t get_index_from_offset(int16_t offset)
{
    uint16_t current_index = abs(offset / pane_offset);
    return current_index;
}

static void cta_l_trains_window_snap_pane(void)
{
    GPoint current_offset = scroll_layer_get_content_offset(cta_l_trains_scroll_layer);

    if (current_offset.y % pane_offset == 0)
        return;

    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Need to snap pane", __func__);

    int floored_offset = (current_offset.y / pane_offset) * pane_offset;
    int offset_difference = current_offset.y - floored_offset;

    int new_offset;
    if (abs(offset_difference) >= pane_offset / 2)
        new_offset = current_offset.y - (pane_offset - abs(offset_difference));
    else
        new_offset = current_offset.y - offset_difference;

    scroll_layer_set_content_offset(cta_l_trains_scroll_layer, (GPoint){0, new_offset}, false);
}

static void cta_l_trains_window_scroll(int8_t direction)
{
    GPoint current_offset = scroll_layer_get_content_offset(cta_l_trains_scroll_layer);

    int16_t new_offset = (direction * pane_offset) + current_offset.y;

    if (new_offset > 0 || new_offset <= max_offset)
        return;                 /* Out of bounds */

    scroll_layer_set_content_offset(cta_l_trains_scroll_layer, (GPoint){0, new_offset}, true);

    /* If scroll layer gets out of sync, snap it back into place */
    cta_l_trains_window_snap_pane();
}

static void cta_l_trains_scroll_layer_up_click_handler(ClickRecognizerRef recognizer, void *context) {
    cta_l_trains_window_scroll(1);
}

static void cta_l_trains_scroll_layer_down_click_handler(ClickRecognizerRef recognizer, void *context) {
    cta_l_trains_window_scroll(-1);
}

static void cta_l_trains_scroll_layer_select_click_handler(ClickRecognizerRef recognizer, void *context) {
    GPoint current_offset = scroll_layer_get_content_offset(cta_l_trains_scroll_layer);
    uint16_t current_index = get_index_from_offset(current_offset.y);

    if (cta_l_trains_window_items[current_index].callback)
        cta_l_trains_window_items[current_index].callback(current_index);
}

static void cta_l_trains_select_callback(uint16_t current_index)
{
    cta_stations_window_push((CtaLineID)current_index);
}

static void cta_l_trains_scroll_layer_click_config_provider(void *context) {
    window_single_click_subscribe(BUTTON_ID_UP,     cta_l_trains_scroll_layer_up_click_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN,   cta_l_trains_scroll_layer_down_click_handler);
    window_single_click_subscribe(BUTTON_ID_SELECT, cta_l_trains_scroll_layer_select_click_handler);
}

static void cta_l_trains_window_deinit_items(void)
{
    for (uint i = 0; i < NUM_CTA_L_TRAINS_WINDOW_ITEMS; i++) {
        gdraw_command_image_destroy(cta_l_trains_window_items[i].prv_image);
        text_layer_destroy(cta_l_trains_window_items[i].prv_text_layer);
        layer_destroy(cta_l_trains_window_items[i].prv_canvas_layer);
        layer_destroy(cta_l_trains_window_items[i].prv_item_layer);
    }
}

static GSize cta_l_trains_window_init_items(GRect bounds)
{
    GRect canvas_bounds = bounds;
    GRect text_layer_bounds = bounds;
    text_layer_bounds.origin.y += 115; /* Magic numbers... */
    text_layer_bounds.origin.x += 33; /* Magic numbers... */
    text_layer_bounds.size.h   -= 115 - STATUS_BAR_LAYER_HEIGHT;

    GSize content_size = {bounds.size.w, 0};

    int16_t offset = 0;
    for (uint i = 0; i < NUM_CTA_L_TRAINS_WINDOW_ITEMS; i++) {
        cta_l_trains_window_items[i].prv_image = gdraw_command_image_create_with_resource(cta_l_trains_window_items[i].resource_id);
        cta_l_trains_window_items[i].prv_item_layer   = layer_create(bounds);
        cta_l_trains_window_items[i].prv_canvas_layer = layer_create_with_data(canvas_bounds, sizeof(CtaLTrainsWindowItem*));
        cta_l_trains_window_items[i].prv_text_layer   = text_layer_create(text_layer_bounds);
        cta_l_trains_window_items[i].prv_offset = offset;

        CtaLTrainsWindowItem** ptr = layer_get_data(cta_l_trains_window_items[i].prv_canvas_layer);
        (*ptr) = &cta_l_trains_window_items[i];

        layer_add_child(cta_l_trains_window_items[i].prv_item_layer, cta_l_trains_window_items[i].prv_canvas_layer);
        layer_add_child(cta_l_trains_window_items[i].prv_item_layer, text_layer_get_layer(cta_l_trains_window_items[i].prv_text_layer));

        text_layer_set_text(cta_l_trains_window_items[i].prv_text_layer, cta_l_trains_window_items[i].title);
        text_layer_set_font(cta_l_trains_window_items[i].prv_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
        text_layer_set_text_alignment(cta_l_trains_window_items[i].prv_text_layer, GTextAlignmentLeft);
        text_layer_set_background_color(cta_l_trains_window_items[i].prv_text_layer, GColorClear);
        text_layer_set_text_color(cta_l_trains_window_items[i].prv_text_layer, GColorBlack);

        layer_set_update_proc(cta_l_trains_window_items[i].prv_canvas_layer, cta_l_trains_window_items_update_proc);

        scroll_layer_add_child(cta_l_trains_scroll_layer, cta_l_trains_window_items[i].prv_item_layer);

        offset -= bounds.size.h;
        bounds.origin.y += bounds.size.h;
        content_size.h  += bounds.size.h;
    }

    max_offset = -1 * content_size.h;

    return content_size;
}

static void cta_l_trains_window_load(Window *window)
{
    Layer *window_layer = window_get_root_layer(window);
    GRect root_bounds = layer_get_bounds(window_layer);

    pane_offset = root_bounds.size.h; /* Height of each pane */

    window_set_background_color(window, CTA_L_TRAINS_WINDOW_BACKGROUND_COLOR);

    cta_l_trains_scroll_layer = scroll_layer_create(root_bounds);

    scroll_layer_set_callbacks(cta_l_trains_scroll_layer, (ScrollLayerCallbacks){
            .click_config_provider = cta_l_trains_scroll_layer_click_config_provider,
        });
    scroll_layer_set_click_config_onto_window(cta_l_trains_scroll_layer, window);
    scroll_layer_set_shadow_hidden(cta_l_trains_scroll_layer, true);

    GSize content_size = cta_l_trains_window_init_items(root_bounds);

    scroll_layer_set_content_size(cta_l_trains_scroll_layer, content_size);

    layer_add_child(window_layer, scroll_layer_get_layer(cta_l_trains_scroll_layer));

    // Get the ContentIndicator from the ScrollLayer
    cta_l_trains_content_indicator = scroll_layer_get_content_indicator(cta_l_trains_scroll_layer);

    // Create two Layers to draw the arrows
    up_content_indicator = layer_create(GRect(root_bounds.origin.x, root_bounds.origin.y,
                                              root_bounds.size.w, STATUS_BAR_LAYER_HEIGHT));
    down_content_indicator = layer_create(GRect(0, root_bounds.size.h - STATUS_BAR_LAYER_HEIGHT,
                                                root_bounds.size.w, STATUS_BAR_LAYER_HEIGHT));
    layer_add_child(window_layer, up_content_indicator);
    layer_add_child(window_layer, down_content_indicator);

    // Configure the properties of each indicator
    ContentIndicatorConfig up_config = (ContentIndicatorConfig) {
        .layer = up_content_indicator,
        .times_out = false,
        .alignment = GAlignCenter,
        .colors = {
            .foreground = GColorWhite,
            .background = CTA_L_TRAINS_WINDOW_BACKGROUND_COLOR
        }
    };
    content_indicator_configure_direction(cta_l_trains_content_indicator, ContentIndicatorDirectionUp, &up_config);

    ContentIndicatorConfig down_config = up_config;
    down_config.layer = down_content_indicator;
    content_indicator_configure_direction(cta_l_trains_content_indicator, ContentIndicatorDirectionDown, &down_config);

    scroll_layer_set_content_offset(cta_l_trains_scroll_layer, GPointZero, false); /* Set scroll layer to top to display arrows */
}

static void cta_l_trains_window_unload(Window *window)
{
    cta_l_trains_window_deinit_items();
    layer_destroy(up_content_indicator);
    layer_destroy(down_content_indicator);
    scroll_layer_destroy(cta_l_trains_scroll_layer);
    window_destroy(window);
}

void cta_l_trains_window_push(void)
{
    cta_l_trains_window = window_create();

    window_set_window_handlers(cta_l_trains_window, (WindowHandlers) {
            .load   = cta_l_trains_window_load,
            .unload = cta_l_trains_window_unload,
        });

    window_stack_push(cta_l_trains_window, true);
}

#endif
