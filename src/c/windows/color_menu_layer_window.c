#include <pebble.h>

#include "lib/color_menu_layer.h"
#include "windows/color_menu_layer_window.h"

typedef struct ColorMenuLayerWindowData {
    ColorMenuLayer *color_menu_layer;
    ColorMenuItem  *items;
    uint16_t        num_items;
    void           *callback_context;
    ColorMenuLayerWindowDestroyCallback destroy_callback;
} ColorMenuLayerWindowData;

static void color_menu_layer_window_load(Window *window)
{
    Layer *root_layer    = window_get_root_layer(window);
    GRect menu_bounds    = layer_get_bounds(root_layer);
    ColorMenuLayerWindowData *color_menu_layer_window_data = window_get_user_data(window);

    ColorMenuLayer *color_menu_layer = color_menu_layer_create(menu_bounds,
                                                               window,
                                                               color_menu_layer_window_data->items,
                                                               color_menu_layer_window_data->num_items,
                                                               color_menu_layer_window_data->callback_context);

    color_menu_layer_window_data->color_menu_layer = color_menu_layer;

    menu_layer_set_click_config_onto_window(color_menu_layer->menu_layer, window);
    layer_add_child(root_layer, menu_layer_get_layer(color_menu_layer->menu_layer));
}

static void color_menu_layer_window_unload(Window *window)
{
    ColorMenuLayerWindowData *color_menu_layer_window_data = window_get_user_data(window);

    color_menu_layer_destroy(color_menu_layer_window_data->color_menu_layer);

    if (color_menu_layer_window_data->destroy_callback)
        color_menu_layer_window_data->destroy_callback(color_menu_layer_window_data->callback_context);

    free(color_menu_layer_window_data);

    window_destroy(window);
}

void color_menu_layer_window_push(ColorMenuItem *menu_items, uint16_t menu_items_count, void *callback_context, ColorMenuLayerWindowDestroyCallback destroy_callback)
{
    Window *color_menu_layer_window = window_create();
    WindowHandlers color_menu_layer_window_handlers = {
        .load   = color_menu_layer_window_load,
        .unload = color_menu_layer_window_unload
    };

    window_set_window_handlers(color_menu_layer_window, color_menu_layer_window_handlers);

    ColorMenuLayerWindowData *color_menu_layer_window_data = calloc(1, sizeof(ColorMenuLayerWindowData));
    color_menu_layer_window_data->callback_context = callback_context;
    color_menu_layer_window_data->destroy_callback = destroy_callback;

    color_menu_layer_window_data->items     = menu_items;
    color_menu_layer_window_data->num_items = menu_items_count;

    window_set_user_data(color_menu_layer_window, color_menu_layer_window_data);

    window_stack_push(color_menu_layer_window, true);
}
