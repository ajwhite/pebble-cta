#include <pebble.h>
#include "windows/cta_tracker_window.h"
#include "windows/no_trains_window.h"
#include "lib/cta_menu_layer.h"
#include "lib/cta_lines.h"

#include "lib/comm.h"

typedef struct CtaTrackerWindowData {
    CtaMenuLayer *cta_menu_layer;
    CtaTrainList *cta_train_list;
} CtaTrackerWindowData;

static void cta_tracker_window_load(Window *window)
{
    Layer *root_layer = window_get_root_layer(window);
    CtaTrackerWindowData *cta_tracker_window_data = window_get_user_data(window);
    CtaMenuLayer *cta_menu_layer = cta_menu_layer_create(window, cta_tracker_window_data->cta_train_list);

    cta_tracker_window_data->cta_menu_layer = cta_menu_layer;

    menu_layer_set_click_config_onto_window(cta_menu_layer->menu_layer, window);
    layer_add_child(root_layer, menu_layer_get_layer(cta_menu_layer->menu_layer));
}

static void cta_tracker_window_unload(Window *window)
{
    CtaTrackerWindowData *cta_tracker_window_data = window_get_user_data(window);

    cta_menu_layer_destroy(cta_tracker_window_data->cta_menu_layer);

    cta_train_list_destroy(cta_tracker_window_data->cta_train_list);

    free(cta_tracker_window_data);

    window_destroy(window);
}

void cta_tracker_window_push(CtaTrainList *train_list)
{
    Window *cta_tracker_window = window_create();
    WindowHandlers cta_tracker_window_handlers = {
        .load   = cta_tracker_window_load,
        .unload = cta_tracker_window_unload
    };

    window_set_window_handlers(cta_tracker_window, cta_tracker_window_handlers);

    CtaTrackerWindowData *cta_tracker_window_data = calloc(1, sizeof(CtaTrackerWindowData));
    cta_tracker_window_data->cta_train_list = train_list;
    window_set_user_data(cta_tracker_window, cta_tracker_window_data);

    window_stack_push(cta_tracker_window, true);
}

static void push_station_transaction_callback(CtaCommTransaction *transaction)
{
    if (transaction->transaction_data.train_list_transaction.cta_train_list == NULL)    /* No trains available */
        no_trains_window_push("No trains available!");
    else
        cta_tracker_window_push(transaction->transaction_data.train_list_transaction.cta_train_list);

    free(transaction);
}

void cta_tracker_window_push_station(int station)
{
    CtaCommTransaction *transaction = calloc(1, sizeof(CtaCommTransaction));

    transaction->transaction_type = CTA_TRAIN_LIST_TRANSACTION;
    transaction->transaction_data.train_list_transaction.cta_station = station;
    transaction->callback = push_station_transaction_callback;

    comm_perform_transaction(transaction);
}
