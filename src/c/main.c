#include <pebble.h>
#include "lib/comm.h"

#include "windows/main_window.h"

int main()
{
    comm_init();

    main_window_push();

    app_event_loop();

    return 0;
}
