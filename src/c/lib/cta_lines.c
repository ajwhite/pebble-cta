#include <pebble.h>
#include "lib/cta_lines.h"

GColor cta_get_primary_color(CtaLineID line)
{
    switch (line) {
        case CTA_red_line:    return GColorRed;
        case CTA_blue_line:   return GColorBlue;
        case CTA_brown_line:  return GColorBulgarianRose;
        case CTA_green_line:  return GColorIslamicGreen;
        case CTA_orange_line: return GColorOrange;
        case CTA_pink_line:   return GColorBrilliantRose;
        case CTA_purple_line: return GColorPurple;
        case CTA_yellow_line: return GColorChromeYellow;
        default:              return GColorBlack;
    }
}

GColor cta_get_secondary_color(CtaLineID line)
{
    return GColorWhite;
}

CtaTrainList *cta_train_list_create(int num_trains)
{
    CtaTrainList *cta_train_list;

    cta_train_list = calloc(1, sizeof(CtaTrainList));

    cta_train_list->train_list = calloc(num_trains, sizeof(CtaTrain));

    cta_train_list->train_count = num_trains;

    return cta_train_list;
}

void cta_train_list_destroy(CtaTrainList *cta_train_list)
{
    free(cta_train_list->train_list);
    free(cta_train_list);
}

#include "cta_stations/red_line_stations.h"
#include "cta_stations/blue_line_stations.h"
#include "cta_stations/brown_line_stations.h"
#include "cta_stations/green_line_stations.h"
#include "cta_stations/orange_line_stations.h"
#include "cta_stations/pink_line_stations.h"
#include "cta_stations/purple_line_stations.h"
#include "cta_stations/yellow_line_stations.h"

static CtaLine cta_lines[] = {
    {
        .name = "Red Line",
        .station_count = sizeof(red_line_stations)/sizeof(CtaStation),
        .stations = red_line_stations,
        .line_id = CTA_red_line,
    },
    {
        .name = "Blue Line",
        .station_count = sizeof(blue_line_stations)/sizeof(CtaStation),
        .stations = blue_line_stations,
        .line_id = CTA_blue_line,
    },
    {
        .name = "Brown Line",
        .station_count = sizeof(brown_line_stations)/sizeof(CtaStation),
        .stations = brown_line_stations,
        .line_id = CTA_brown_line,
    },
    {
        .name = "Green Line",
        .station_count = sizeof(green_line_stations)/sizeof(CtaStation),
        .stations = green_line_stations,
        .line_id = CTA_green_line,
    },
    {
        .name = "Orange Line",
        .station_count = sizeof(orange_line_stations)/sizeof(CtaStation),
        .stations = orange_line_stations,
        .line_id = CTA_orange_line,
    },
    {
        .name = "Pink Line",
        .station_count = sizeof(pink_line_stations)/sizeof(CtaStation),
        .stations = pink_line_stations,
        .line_id = CTA_pink_line,
    },
    {
        .name = "Purple Line",
        .station_count = sizeof(purple_line_stations)/sizeof(CtaStation),
        .stations = purple_line_stations,
        .line_id = CTA_purple_line,
    },
    {
        .name = "Yellow Line",
        .station_count = sizeof(yellow_line_stations)/sizeof(CtaStation),
        .stations = yellow_line_stations,
        .line_id = CTA_yellow_line,
    },
};

CtaLine *cta_stations_get_line(CtaLineID line_id)
{
    if (line_id > CTA_yellow_line)
        return NULL;

    return &cta_lines[line_id];
}
