#include <pebble.h>
#include "lib/color_menu_layer.h"

#define TEXT_ALIGNMENT PBL_IF_ROUND_ELSE(GTextAlignmentCenter, GTextAlignmentLeft)

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *callback_context)
{
    ColorMenuLayer *color_menu_layer = (ColorMenuLayer *)callback_context;

    return color_menu_layer->num_items;
}

static void menu_draw_highlighted_row(GContext *ctx, GRect bounds, ColorMenuItem *item)
{
    graphics_context_set_fill_color(ctx, item->primary_color.argb ? item->primary_color : GColorBlack);
    graphics_fill_rect(ctx, bounds, 0, 0);
    
    graphics_context_set_text_color(ctx, item->secondary_color.argb ? item->secondary_color : GColorWhite);
    graphics_draw_text(ctx,
                       item->title,
                       fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD),
                       GRect(10, 5, bounds.size.w - 10, bounds.size.h - 5),
                       GTextOverflowModeTrailingEllipsis,
                       TEXT_ALIGNMENT,
                       NULL);
}

static void menu_draw_other_row(GContext *ctx, GRect bounds, ColorMenuItem *item)
{
    graphics_context_set_fill_color(ctx, item->secondary_color.argb ? item->secondary_color : GColorWhite);
    graphics_fill_rect(ctx, bounds, 0, 0);
    
    graphics_context_set_text_color(ctx, item->primary_color.argb ? item->primary_color : GColorBlack);

    graphics_draw_text(ctx,
                       item->title,
                       fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD),
                       GRect(10, 5, bounds.size.w - 10, bounds.size.h - 5),
                       GTextOverflowModeTrailingEllipsis,
                       TEXT_ALIGNMENT,
                       NULL);
}

static void menu_draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context)
{
    ColorMenuLayer *color_menu_layer = (ColorMenuLayer *)callback_context;
    GRect bounds = layer_get_bounds(cell_layer);

    if (menu_cell_layer_is_highlighted(cell_layer)) {
        menu_draw_highlighted_row(ctx, bounds, &color_menu_layer->items[cell_index->row]);
    } else {
        menu_draw_other_row(ctx, bounds, &color_menu_layer->items[cell_index->row]);
    }
}

static void menu_select_click_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context)
{
    ColorMenuLayer *color_menu_layer = (ColorMenuLayer *)callback_context;

    if (color_menu_layer->items[cell_index->row].callback)
        color_menu_layer->items[cell_index->row].callback(cell_index->row, callback_context);
}

void color_menu_layer_set_callbacks(ColorMenuLayer *color_menu_layer, void *callback_context, MenuLayerCallbacks callbacks)
{
    color_menu_layer->callback_context = callback_context;

    MenuLayerCallbacks menu_layer_callbacks = callbacks;
    menu_layer_callbacks.get_num_rows = callbacks.get_num_rows ? callbacks.get_num_rows : menu_get_num_rows_callback;
    menu_layer_callbacks.draw_row     = callbacks.draw_row     ? callbacks.draw_row     : menu_draw_row_callback;
    menu_layer_callbacks.select_click = callbacks.select_click ? callbacks.select_click : menu_select_click_callback;

    menu_layer_set_callbacks(color_menu_layer->menu_layer, color_menu_layer, menu_layer_callbacks);
}

ColorMenuLayer *color_menu_layer_create(GRect frame, Window *window, ColorMenuItem *items, uint16_t num_items, void *callback_context)
{
    ColorMenuLayer *color_menu_layer = calloc(1, sizeof(ColorMenuLayer));

    color_menu_layer->menu_layer       = menu_layer_create(frame);
    color_menu_layer->num_items        = num_items;
    color_menu_layer->items            = items;
    color_menu_layer->callback_context = callback_context;

    menu_layer_set_callbacks(color_menu_layer->menu_layer, color_menu_layer, (MenuLayerCallbacks) {
            .get_num_rows          = menu_get_num_rows_callback,
            .draw_row              = menu_draw_row_callback,
            .get_cell_height       = NULL,
            .select_click          = menu_select_click_callback,
            .select_long_click     = NULL,
            .get_header_height     = NULL,
            .draw_header           = NULL,
            .get_num_sections      = NULL,
            .selection_changed     = NULL,
            .get_separator_height  = NULL,
            .draw_separator        = NULL,
            .selection_will_change = NULL,
            .draw_background       = NULL,
        });

    return color_menu_layer;
}

void color_menu_layer_destroy(ColorMenuLayer *color_menu_layer)
{
    menu_layer_destroy(color_menu_layer->menu_layer);
    free(color_menu_layer);
}
