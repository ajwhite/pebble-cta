#include <pebble.h>
#include "lib/comm.h"
#include "lib/cta_lines.h"
#include "lib/favorites.h"
#include "windows/wait_for_network_window.h"

static bool pebblekit_js_is_ready      = false;
static bool transaction_in_progress    = false;

static CtaCommTransaction *repeat_transaction = NULL;

#define INBOX_SIZE 64
#define OUTBOX_SIZE 64

static void comm_initiate_transacton(CtaCommTransaction *transaction);

static void comm_repeat_transaction(void)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    if (!repeat_transaction)
        return;

    app_message_set_context(repeat_transaction);
    comm_initiate_transacton(repeat_transaction);
    repeat_transaction = NULL;

    wait_for_network_window_destroy();
}

static void inbox_received_callback(DictionaryIterator *iter, void *context)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    Tuple *ready_tuple = dict_find(iter, MESSAGE_KEY_js_ready);
    if (ready_tuple) {
        APP_LOG(APP_LOG_LEVEL_INFO, "%s: PebbleKit JS ready!", __func__);
        pebblekit_js_is_ready = true;

        comm_repeat_transaction();
    }

    if (context == NULL)
        return;

    CtaCommTransaction *transaction = (CtaCommTransaction *)context;

    /* =========================== */
    /* Favorites ================= */
    /* =========================== */
    if (transaction->transaction_type == CTA_FAVORITES_TRANSACTION) {
        /* End of favorites transction */
        Tuple *all_favorites_sent_tuple = dict_find(iter, MESSAGE_KEY_all_favorites_sent);
        if (all_favorites_sent_tuple) {
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: All favorites received", __func__);
            transaction_in_progress = false;

            app_message_set_context(NULL);

            transaction->callback(transaction);
            return;
        }

        /* First message of new favorites transaction */
        Tuple *favorites_count_tuple = dict_find(iter, MESSAGE_KEY_favorite_count);
        if (favorites_count_tuple) {
            transaction_in_progress = true;

            int favorites_count = favorites_count_tuple->value[0].int16;

            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Favorites count: %d", __func__, favorites_count);

            /* If no favorites are available, end the transaction and don't allocate any memory */
            if (favorites_count == 0) {
                transaction->transaction_data.favorites_transaction.favorites_list = NULL;
                transaction->transaction_done = true;
                transaction_in_progress = false;
                transaction->callback(transaction);
                return;
            }

            /* Allocate and set up favorites list */
            transaction->transaction_data.favorites_transaction.favorites_list = favorites_list_create(favorites_count);
            transaction->transaction_index = 0;
            transaction->transaction_done = false;

            return;
        }

        Favorite *favorites = transaction->transaction_data.favorites_transaction.favorites_list->favorites;
        int8_t favorite_index = transaction->transaction_index++;

        Tuple *favorites_name_tuple = dict_find(iter, MESSAGE_KEY_favorite_name);
        if (favorites_name_tuple) {
            char *favorite_name = favorites_name_tuple->value[0].cstring;
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Favorite name: %s", __func__, favorite_name);
            strncpy(favorites[favorite_index].name, favorite_name, FAVORITES_NAME_SIZE);
        }

        Tuple *favorites_id_tuple = dict_find(iter, MESSAGE_KEY_favorite_id);
        if (favorites_id_tuple) {
            uint32_t favorite_id = favorites_id_tuple->value[0].uint32;
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Favorite station ID: %d", __func__, favorite_id);
            favorites[favorite_index].station_id = favorite_id;
        }
    }


    /* =========================== */
    /* Trains ==================== */
    /* =========================== */
    if (transaction->transaction_type == CTA_TRAIN_LIST_TRANSACTION) {
        /* End of trains list transaction */
        Tuple *all_trains_sent_tuple = dict_find(iter, MESSAGE_KEY_all_trains_sent);
        if (all_trains_sent_tuple) {
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: All trains receieved", __func__);
            transaction_in_progress = false;

            app_message_set_context(NULL);

            transaction->callback(transaction);
            return;
        }

        /* First message of new train list transaction */
        Tuple *train_count_tuple = dict_find(iter, MESSAGE_KEY_train_count);
        if (train_count_tuple) {
            transaction_in_progress = true;

            int train_count = train_count_tuple->value[0].int16;

            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Train count: %d", __func__, train_count);

            /* If no trains are available, end the transaction and don't allocate any memory */
            if (train_count == 0) {
                transaction->transaction_data.train_list_transaction.cta_train_list = NULL;
                transaction->transaction_done = true;
                transaction_in_progress = false;
                transaction->callback(transaction);
                return;
            }

            /* Allocate and set up train list */
            transaction->transaction_data.train_list_transaction.cta_train_list = cta_train_list_create(train_count);
            transaction->transaction_data.train_list_transaction.cta_train_list->train_count = train_count;
            transaction->transaction_index = 0;
            transaction->transaction_done = false;

            return;
        }

        CtaTrain *trains = transaction->transaction_data.train_list_transaction.cta_train_list->train_list;
        int8_t train_index = transaction->transaction_index++;

        Tuple *terminus_tuple = dict_find(iter, MESSAGE_KEY_terminus);
        if (terminus_tuple) {
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Terminus: %s", __func__, terminus_tuple->value[0].cstring);
            strncpy(trains[train_index].terminus, terminus_tuple->value[0].cstring, sizeof(trains[train_index].terminus));
        }

        Tuple *line_tuple = dict_find(iter, MESSAGE_KEY_line);
        if (line_tuple) {
            trains[train_index].line = line_tuple->value[0].int8;
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Line: %d", __func__, trains[train_index].line);
        }

        Tuple *due_time_tuple = dict_find(iter, MESSAGE_KEY_due_time);
        if (due_time_tuple) {
            trains[train_index].due_time = due_time_tuple->value[0].int16;
            APP_LOG(APP_LOG_LEVEL_INFO, "%s: Due time: %d", __func__, trains[train_index].due_time);
        }
    }
}

static void inbox_dropped_callback(AppMessageResult reason, void *context) {
    // A message was received, but had to be dropped
    APP_LOG(APP_LOG_LEVEL_ERROR, "Message dropped. Reason: %d", (int)reason);
}

static void outbox_sent_callback(DictionaryIterator *iter, void *context) {
    // The message just sent has been successfully delivered
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
}

static void outbox_failed_callback(DictionaryIterator *iter, AppMessageResult reason, void *context) {
    // The message just sent failed to be delivered
    APP_LOG(APP_LOG_LEVEL_ERROR, "Message send failed. Reason: %d", (int)reason);
}

void comm_init(void)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    app_message_open(INBOX_SIZE, OUTBOX_SIZE);

    app_message_set_context(NULL);

    app_message_register_inbox_received(inbox_received_callback);
    app_message_register_inbox_dropped(inbox_dropped_callback);
    app_message_register_outbox_sent(outbox_sent_callback);
    app_message_register_outbox_failed(outbox_failed_callback);
}

static void comm_initiate_transacton(CtaCommTransaction *transaction)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    DictionaryIterator *out_iter;
    AppMessageResult result;

    transaction->transaction_index = 0;
    transaction->transaction_done = false;

    result = app_message_outbox_begin(&out_iter);
    if(result == APP_MSG_OK) {
        if (transaction->transaction_type == CTA_TRAIN_LIST_TRANSACTION)
            dict_write_int32(out_iter, MESSAGE_KEY_station, transaction->transaction_data.train_list_transaction.cta_station);
        else
            dict_write_int16(out_iter, MESSAGE_KEY_favorite_count, 1);
    } else {
        APP_LOG(APP_LOG_LEVEL_ERROR, "Error preparing the outbox: %d", (int)result);
    }

    result = app_message_outbox_send();
    if(result != APP_MSG_OK) {
        APP_LOG(APP_LOG_LEVEL_ERROR, "Error sending the outbox: %d", (int)result);
    }
}

bool comm_perform_transaction(CtaCommTransaction *transaction)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    if (!pebblekit_js_is_ready) {
        APP_LOG(APP_LOG_LEVEL_ERROR, "PebbleKit JS is not ready");
        repeat_transaction = transaction;
        wait_for_network_window_push();
        return true;
    }

    if (transaction_in_progress) {
        APP_LOG(APP_LOG_LEVEL_ERROR, "Transaction currently in progress");
        return false;
    }

    app_message_set_context(transaction);

    comm_initiate_transacton(transaction);

    return true;
}
