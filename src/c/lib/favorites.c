#include <pebble.h>
#include "lib/favorites.h"

void favorites_list_destroy(FavoritesList *favorites_list)
{
    free(favorites_list->favorites);
    free(favorites_list);
}

FavoritesList* favorites_list_create(int16_t favorites_count)
{
    FavoritesList *favorites_list;

    favorites_list = calloc(1, sizeof(FavoritesList));

    favorites_list->favorites = calloc(favorites_count, sizeof(Favorite));

    favorites_list->favorites_count = favorites_count;

    return favorites_list;
}
