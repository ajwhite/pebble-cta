#include <pebble.h>
#include "lib/cta_menu_layer.h"
#include "lib/cta_lines.h"

static uint16_t menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *callback_context)
{
    CtaMenuLayer *cta_menu_layer = (CtaMenuLayer *)callback_context;

    return cta_menu_layer->train_list->train_count;
}

static void menu_draw_highlighted_row(GContext *ctx, GRect bounds, CtaTrain *current_train)
{
    GColor primary_color   = cta_get_primary_color(current_train->line);
    GColor secondary_color = cta_get_secondary_color(current_train->line);
    graphics_context_set_fill_color(ctx, primary_color);
    graphics_fill_rect(ctx, bounds, 0, 0);

    graphics_context_set_text_color(ctx, secondary_color);

    graphics_draw_text(ctx,
                       current_train->terminus,
                       fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD),
                       GRect(0, 0, bounds.size.w, 30),
                       GTextOverflowModeTrailingEllipsis,
                       GTextAlignmentCenter,
                       NULL);

    char due_time[] = "99 min  ";
    snprintf(due_time, sizeof(due_time),
             current_train->due_time ? "%d min" : "Due",
             current_train->due_time);

    graphics_draw_text(ctx,
                       due_time,
                       fonts_get_system_font(FONT_KEY_GOTHIC_28),
                       GRect(0, 30, bounds.size.w, 30),
                       GTextOverflowModeTrailingEllipsis,
                       GTextAlignmentCenter,
                       NULL);
}

static void menu_draw_other_row(GContext *ctx, GRect bounds, CtaTrain *current_train)
{
    GColor primary_color   = cta_get_primary_color(current_train->line);
    GColor secondary_color = cta_get_secondary_color(current_train->line);
    graphics_context_set_fill_color(ctx, secondary_color);
    graphics_fill_rect(ctx, bounds, 0, 0);

    graphics_context_set_text_color(ctx, primary_color);

    graphics_draw_text(ctx,
                       current_train->terminus,
                       fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD),
                       GRect(0, 0, bounds.size.w, MENU_CELL_ROUND_UNFOCUSED_SHORT_CELL_HEIGHT),
                       GTextOverflowModeTrailingEllipsis,
                       GTextAlignmentCenter,
                       NULL);
}

static void menu_draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context)
{
    CtaMenuLayer *cta_menu_layer = (CtaMenuLayer *)callback_context;
    CtaTrainList *cta_train_list = cta_menu_layer->train_list;
    CtaTrain *current_train = &cta_train_list->train_list[cell_index->row];

    GRect bounds = layer_get_bounds(cell_layer);

    if (menu_cell_layer_is_highlighted(cell_layer)) {
        menu_draw_highlighted_row(ctx, bounds, current_train);
    } else {
        menu_draw_other_row(ctx, bounds, current_train);
    }
}

static int16_t menu_get_cell_height_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context)
{
    if (menu_layer_is_index_selected(menu_layer, cell_index))
        return MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT;
    else
        return MENU_CELL_ROUND_UNFOCUSED_SHORT_CELL_HEIGHT;
}

CtaMenuLayer *cta_menu_layer_create(Window *window, CtaTrainList *cta_train_list)
{
    Layer *root_layer    = window_get_root_layer(window);
    GRect menu_bounds    = layer_get_bounds(root_layer);

    CtaMenuLayer *cta_menu_layer = malloc(sizeof(CtaMenuLayer));
    cta_menu_layer->menu_layer = menu_layer_create(menu_bounds);
    cta_menu_layer->train_list = cta_train_list;

    menu_layer_set_center_focused(cta_menu_layer->menu_layer, true);

    menu_layer_set_callbacks(cta_menu_layer->menu_layer, cta_menu_layer, (MenuLayerCallbacks) {
            .get_num_rows          = menu_get_num_rows_callback,         /* NOTE: Required callback */
            .draw_row              = menu_draw_row_callback,             /* NOTE: Required callback */
            .get_cell_height       = menu_get_cell_height_callback,
            .select_click          = NULL,
            .select_long_click     = NULL,
            .get_header_height     = NULL,
            .draw_header           = NULL,                               /* NOTE: Must be valid callback if .get_header_height is not NULL */
            .get_num_sections      = NULL,
            .selection_changed     = NULL,
            .get_separator_height  = NULL,
            .draw_separator        = NULL,
            .selection_will_change = NULL,
            .draw_background       = NULL,
        });

    return cta_menu_layer;
}

void cta_menu_layer_destroy(CtaMenuLayer *cta_menu_layer)
{
    menu_layer_destroy(cta_menu_layer->menu_layer);
    free(cta_menu_layer);
}
