static CtaStation brown_line_stations[] = {
    {
        .name = "Kimball",
        .station_id = 41290,
    },
    {
        .name = "Kedzie",
        .station_id = 41180,
    },
    {
        .name = "Francisco",
        .station_id = 40870,
    },
    {
        .name = "Rockwell",
        .station_id = 41010,
    },
    {
        .name = "Western",
        .station_id = 41480,
    },
    {
        .name = "Damen",
        .station_id = 40090,
    },
    {
        .name = "Montrose",
        .station_id = 41500,
    },
    {
        .name = "Irving Park",
        .station_id = 41460,
    },
    {
        .name = "Addison",
        .station_id = 41440,
    },
    {
        .name = "Paulina",
        .station_id = 41310,
    },
    {
        .name = "Southport",
        .station_id = 40360,
    },
    {
        .name = "Belmont",
        .station_id = 41320,
    },
    {
        .name = "Wellington",
        .station_id = 41210,
    },
    {
        .name = "Diversey",
        .station_id = 40530,
    },
    {
        .name = "Fullerton",
        .station_id = 41220,
    },
    {
        .name = "Armitage",
        .station_id = 40660,
    },
    {
        .name = "Sedgewick",
        .station_id = 40800,
    },
    {
        .name = "Chicago",
        .station_id = 40710,
    },
    {
        .name = "Merchandise Mart",
        .station_id = 40460,
    },
    {
        .name = "Washington/Wells",
        .station_id = 40730,
    },
    {
        .name = "Quincy",
        .station_id = 40040,
    },
    {
        .name = "LaSalle/Van Buren",
        .station_id = 40160,
    },
    {
        .name = "Harold Washington Library-State/Van Burenm",
        .station_id = 40850,
    },
    {
        .name = "Adams/Wabash",
        .station_id = 40680,
    },
    {
        .name = "Washington/Wabash",
        .station_id = 41700,
    },
    {
        .name = "State/Lake",
        .station_id = 40260,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
};
