static CtaStation red_line_stations[] = {
    {
        .name = "Howard",
        .station_id = 40900,
    },
    {
        .name = "Jarvis",
        .station_id = 41190,
    },
    {
        .name = "Morse",
        .station_id = 40100,
    },
    {
        .name = "Loyola",
        .station_id = 41300,
    },
    {
        .name = "Granville",
        .station_id = 40760,
    },
    {
        .name = "Thorndale",
        .station_id = 40880,
    },
    {
        .name = "Bryn Mawr",
        .station_id = 41380,
    },
    {
        .name = "Berwyn",
        .station_id = 40340,
    },
    {
        .name = "Argyle",
        .station_id = 41200,
    },
    {
        .name = "Lawrence",
        .station_id = 40770,
    },
    {
        .name = "Wilson",
        .station_id = 40540,
    },
    {
        .name = "Sheridan",
        .station_id = 40080,
    },
    {
        .name = "Addison",
        .station_id = 41420,
    },
    {
        .name = "Belmont",
        .station_id = 41320,
    },
    {
        .name = "Fullerton",
        .station_id = 41220,
    },
    {
        .name = "North/Clybourn",
        .station_id = 40650,
    },
    {
        .name = "Clark/Division",
        .station_id = 40630,
    },
    {
        .name = "Chicago",
        .station_id = 41450,
    },
    {
        .name = "Grand",
        .station_id = 40330,
    },
    {
        .name = "Lake",
        .station_id = 41660,
    },
    {
        .name = "Monroe",
        .station_id = 41090,
    },
    {
        .name = "Jackson",
        .station_id = 40560,
    },
    {
        .name = "Harrison",
        .station_id = 41490,
    },
    {
        .name = "Roosevelt",
        .station_id = 41400,
    },
    {
        .name = "Cermak-Chinatown",
        .station_id = 41000,
    },
    {
        .name = "Sox-35th",
        .station_id = 40190,
    },
    {
        .name = "47th",
        .station_id = 41230,
    },
    {
        .name = "Garfield",
        .station_id = 41170,
    },
    {
        .name = "63rd",
        .station_id = 40910,
    },
    {
        .name = "69th",
        .station_id = 40990,
    },
    {
        .name = "79th",
        .station_id = 40240,
    },
    {
        .name = "87th",
        .station_id = 41430,
    },
    {
        .name = "95th/Dan Ryan",
        .station_id = 40450,
    },
};
