static CtaStation purple_line_stations[] = {
    {
        .name = "Linden",
        .station_id = 41050,
    },
    {
        .name = "Central",
        .station_id = 41250,
    },
    {
        .name = "Noyes",
        .station_id = 40400,
    },
    {
        .name = "Foster",
        .station_id = 40520,
    },
    {
        .name = "Davis",
        .station_id = 40050,
    },
    {
        .name = "Dempster",
        .station_id = 40690,
    },
    {
        .name = "Main",
        .station_id = 40270,
    },
    {
        .name = "South Blvd",
        .station_id = 40840,
    },
    {
        .name = "Howard",
        .station_id = 40900,
    },
    {
        .name = "Wilson",
        .station_id = 40540,
    },
    {
        .name = "Belmont",
        .station_id = 41320,
    },
    {
        .name = "Wellington",
        .station_id = 41210,
    },
    {
        .name = "Diversey",
        .station_id = 40530,
    },
    {
        .name = "Fullerton",
        .station_id = 41220,
    },
    {
        .name = "Armitage",
        .station_id = 40660,
    },
    {
        .name = "Sedgewick",
        .station_id = 40800,
    },
    {
        .name = "Chicago",
        .station_id = 40710,
    },
    {
        .name = "Merchandise Mart",
        .station_id = 40460,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
    {
        .name = "State/Lake",
        .station_id = 40260,
    },
    {
        .name = "Washington/Wabash",
        .station_id = 41700,
    },
    {
        .name = "Adams/Wabash",
        .station_id = 40680,
    },
    {
        .name = "Harold Washington Library-State/Van Buren",
        .station_id = 40850,
    },
    {
        .name = "LaSalle/Van Buren",
        .station_id = 40160,
    },
    {
        .name = "Quincy",
        .station_id = 40040,
    },
    {
        .name = "Washington/Wells",
        .station_id = 40730,
    },
};
