static CtaStation blue_line_stations[] = {
    {
        .name = "O'Hare",
        .station_id = 40890,
    },
    {
        .name = "Rosemont",
        .station_id = 40820,
    },
    {
        .name = "Cumberland",
        .station_id = 40230,
    },
    {
        .name = "Harlem (O'Hare Branch)",
        .station_id = 40750,
    },
    {
        .name = "Jefferson Park",
        .station_id = 41280,
    },
    {
        .name = "Montrose",
        .station_id = 41330,
    },
    {
        .name = "Irving Park",
        .station_id = 40550,
    },
    {
        .name = "Addison",
        .station_id = 41240,
    },
    {
        .name = "Belmont",
        .station_id = 40060,
    },
    {
        .name = "Logan Square",
        .station_id = 41020,
    },
    {
        .name = "California",
        .station_id = 40570,
    },
    {
        .name = "Western (O'Hare Branch)",
        .station_id = 40670,
    },
    {
        .name = "Damen",
        .station_id = 40590,
    },
    {
        .name = "Division",
        .station_id = 40320,
    },
    {
        .name = "Chicago",
        .station_id = 41410,
    },
    {
        .name = "Grand",
        .station_id = 40490,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
    {
        .name = "Washington",
        .station_id = 40370,
    },
    {
        .name = "Monroe",
        .station_id = 40790,
    },
    {
        .name = "Jackson",
        .station_id = 40070,
    },
    {
        .name = "LaSalle",
        .station_id = 41340,
    },
    {
        .name = "Clinton",
        .station_id = 40430,
    },
    {
        .name = "UIC-Halsted",
        .station_id = 40350,
    },
    {
        .name = "Racine",
        .station_id = 40470,
    },
    {
        .name = "Illinois Medical District",
        .station_id = 40810,
    },
    {
        .name = "Western (Forest Park Branch)",
        .station_id = 40220,
    },
    {
        .name = "Kedzie-Homan",
        .station_id = 40250,
    },
    {
        .name = "Pulaski",
        .station_id = 40920,
    },
    {
        .name = "Cicero",
        .station_id = 40970,
    },
    {
        .name = "Austin",
        .station_id = 40010,
    },
    {
        .name = "Oak Park",
        .station_id = 40180,
    },
    {
        .name = "Harlem (Forest Park Branch)",
        .station_id = 40980,
    },
    {
        .name = "Forest Park",
        .station_id = 40390,
    },
};
