static CtaStation green_line_stations[] = {
    {
        .name = "Harlem/Lake",
        .station_id = 40020,
    },
    {
        .name = "Oak Park",
        .station_id = 41350,
    },
    {
        .name = "Ridgeland",
        .station_id = 40610,
    },
    {
        .name = "Austin",
        .station_id = 41260,
    },
    {
        .name = "Central",
        .station_id = 40280,
    },
    {
        .name = "Laramie",
        .station_id = 40700,
    },
    {
        .name = "Cicero",
        .station_id = 40480,
    },
    {
        .name = "Pulaski",
        .station_id = 40030,
    },
    {
        .name = "Conservatory-Central Park Drive",
        .station_id = 41670,
    },
    {
        .name = "Kedzie",
        .station_id = 41070,
    },
    {
        .name = "California",
        .station_id = 41360,
    },
    {
        .name = "Ashland",
        .station_id = 40170,
    },
    {
        .name = "Morgan",
        .station_id = 41510,
    },
    {
        .name = "Clinton",
        .station_id = 41160,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
    {
        .name = "State/Lake",
        .station_id = 40260,
    },
    {
        .name = "Washington/Wabash",
        .station_id = 41700,
    },
    {
        .name = "Adams/Wabash",
        .station_id = 40680,
    },
    {
        .name = "Roosevelt",
        .station_id = 41400,
    },
    {
        .name = "Cermak-McCormick Place",
        .station_id = 41690,
    },
    {
        .name = "35th-Bronzeville-IIT",
        .station_id = 41120,
    },
    {
        .name = "Indiana",
        .station_id = 40300,
    },
    {
        .name = "43rd",
        .station_id = 41270,
    },
    {
        .name = "47th",
        .station_id = 41080,
    },
    {
        .name = "51st",
        .station_id = 40130,
    },
    {
        .name = "Garfield",
        .station_id = 40510,
    },
    {
        .name = "King Drive",
        .station_id = 41140,
    },
    {
        .name = "Cottage Grove",
        .station_id = 40720,
    },
    {
        .name = "Halsted",
        .station_id = 40940,
    },
    {
        .name = "Ashland/63rd",
        .station_id = 40290,
    },
};
