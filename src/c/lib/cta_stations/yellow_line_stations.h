static CtaStation yellow_line_stations[] = {
    {
        .name = "Dempster-Skokie",
        .station_id = 40140,
    },
    {
        .name = "Oakton-Skokie",
        .station_id = 41680,
    },
    {
        .name = "Howard",
        .station_id = 40900,
    },
};
