static CtaStation orange_line_stations[] = {
    {
        .name = "Midway",
        .station_id = 40930,
    },
    {
        .name = "Pulaski",
        .station_id = 40960,
    },
    {
        .name = "Kedzie",
        .station_id = 41150,
    },
    {
        .name = "Western",
        .station_id = 40310,
    },
    {
        .name = "35th/Archer",
        .station_id = 40120,
    },
    {
        .name = "Ashland",
        .station_id = 41060,
    },
    {
        .name = "Halsted",
        .station_id = 41130,
    },
    {
        .name = "Roosevelt",
        .station_id = 41400,
    },
    {
        .name = "Harold Washington Library-State/Van Buren",
        .station_id = 40850,
    },
    {
        .name = "LaSalle/Van Buren",
        .station_id = 40160,
    },
    {
        .name = "Quincy",
        .station_id = 40040,
    },
    {
        .name = "Washington/Wells",
        .station_id = 40730,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
    {
        .name = "State/Lake",
        .station_id = 40260,
    },
    {
        .name = "Washington/Wabash",
        .station_id = 41700,
    },
    {
        .name = "Adams/Wabash",
        .station_id = 40680,
    },
};
