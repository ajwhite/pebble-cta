static CtaStation pink_line_stations[] = {
    {
        .name = "54th/Cermak",
        .station_id = 40580,
    },
    {
        .name = "Cicero",
        .station_id = 40420,
    },
    {
        .name = "Kostner",
        .station_id = 40600,
    },
    {
        .name = "Pulaski",
        .station_id = 40150,
    },
    {
        .name = "Central Park",
        .station_id = 40780,
    },
    {
        .name = "Kedzie",
        .station_id = 41040,
    },
    {
        .name = "California",
        .station_id = 40440,
    },
    {
        .name = "Western",
        .station_id = 40740,
    },
    {
        .name = "Damen",
        .station_id = 40210,
    },
    {
        .name = "18th",
        .station_id = 40830,
    },
    {
        .name = "Polk",
        .station_id = 41030,
    },
    {
        .name = "Ashland",
        .station_id = 40170,
    },
    {
        .name = "Morgan",
        .station_id = 41510,
    },
    {
        .name = "Clinton",
        .station_id = 41160,
    },
    {
        .name = "Clark/Lake",
        .station_id = 40380,
    },
    {
        .name = "State/Lake",
        .station_id = 40260,
    },
    {
        .name = "Washington/Wabash",
        .station_id = 41700,
    },
    {
        .name = "Adams/Wabash",
        .station_id = 40680,
    },
    {
        .name = "Harold Washington Library-State/Van Buren",
        .station_id = 40850,
    },
    {
        .name = "LaSalle/Van Buren",
        .station_id = 40160,
    },
    {
        .name = "Quincy",
        .station_id = 40040,
    },
    {
        .name = "Washington/Wells",
        .station_id = 40730,
    },
};
