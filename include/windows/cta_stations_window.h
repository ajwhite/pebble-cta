#ifndef CTA_STATIONS_WINDOW_H
#define CTA_STATIONS_WINDOW_H

#include "lib/cta_lines.h"

void cta_stations_window_push(CtaLineID line_id);

#endif /* CTA_STATIONS_WINDOW_H */
