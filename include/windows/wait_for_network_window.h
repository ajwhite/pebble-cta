#ifndef WAIT_FOR_NETWORK_WINDOW_H
#define WAIT_FOR_NETWORK_WINDOW_H

void wait_for_network_window_push(void);
void wait_for_network_window_destroy(void);

#endif /* WAIT_FOR_NETWORK_WINDOW_H */
