#ifndef CTA_TRACKER_WINDOW_H
#define CTA_TRACKER_WINDOW_H

#include "../lib/cta_lines.h"

void cta_tracker_window_push(CtaTrainList *train_list);
void cta_tracker_window_push_station(int station);

#endif /* CTA_TRACKER_WINDOW_H */
