#ifndef COLOR_MENU_LAYER_WINDOW_H
#define COLOR_MENU_LAYER_WINDOW_H

#include "lib/color_menu_layer.h"

typedef void (*ColorMenuLayerWindowDestroyCallback)(void *callback_context);

typedef struct ColorMenuLayerWindowPushData {
    void *callback_context;
    ColorMenuLayerWindowDestroyCallback destroy_callback;
} ColorMenuLayerWindowPushData;

void color_menu_layer_window_push(ColorMenuItem *menu_items, uint16_t menu_items_count, void *callback_context, ColorMenuLayerWindowDestroyCallback destroy_callback);

#endif /* COLOR_MENU_LAYER_WINDOW_H */
