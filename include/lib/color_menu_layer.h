#ifndef COLOR_MENU_LAYER_H
#define COLOR_MENU_LAYER_H

#include <pebble.h>

typedef struct ColorMenuItem {
    char *title;
    GColor primary_color;
    GColor secondary_color;
    SimpleMenuLayerSelectCallback callback;
} ColorMenuItem;

typedef struct ColorMenuLayer {
    MenuLayer *menu_layer;
    uint16_t   num_items;
    void      *callback_context;
    ColorMenuItem *items;
} ColorMenuLayer;

ColorMenuLayer *color_menu_layer_create(GRect frame, Window *window, ColorMenuItem *items, uint16_t num_sections, void *callback_context);
void color_menu_layer_destroy(ColorMenuLayer *color_menu_layer);
void color_menu_layer_set_callbacks(ColorMenuLayer *color_menu_layer, void *callback_context, MenuLayerCallbacks callbacks);

#endif /* COLOR_MENU_LAYER_H */
