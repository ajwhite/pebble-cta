#ifndef CTA_LINES_H
#define CTA_LINES_H

#include <pebble.h>

typedef struct CtaStation {
    char    *name;
    uint32_t station_id;
} CtaStation;

typedef enum CtaLineID {
    CTA_red_line = 0,
    CTA_blue_line,
    CTA_brown_line,
    CTA_green_line,
    CTA_orange_line,
    CTA_pink_line,
    CTA_purple_line,
    CTA_yellow_line,
} CtaLineID;

typedef struct CtaLine {
    char *name;
    int  station_count;
    CtaStation *stations;
    CtaLineID line_id;
} CtaLine;

typedef struct CtaTrain {
    char     terminus[128];
    CtaLineID  line;
    uint8_t  due_time;
    uint16_t train_number;
} CtaTrain;

typedef struct CtaTrainList {
    uint8_t train_count;
    struct CtaTrain *train_list;
} CtaTrainList;


GColor cta_get_primary_color(CtaLineID line);
GColor cta_get_secondary_color(CtaLineID line);

CtaTrainList *cta_train_list_create(int num_trains);
void cta_train_list_destroy(CtaTrainList *cta_train_list);

CtaLine *cta_stations_get_line(CtaLineID line_id);

#endif /* CTA_LINES_H */
