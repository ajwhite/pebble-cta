#ifndef COMM_H
#define COMM_H

#include "lib/cta_lines.h"
#include "lib/favorites.h"

struct CtaCommTransaction;

typedef void (*CtaCommPerformTransactionCallback)(struct CtaCommTransaction *transaction);

enum CtaTransactionType {
    CTA_TRAIN_LIST_TRANSACTION,
    CTA_FAVORITES_TRANSACTION
};

typedef struct CtaTrainListTransaction {
    int32_t cta_station;
    CtaTrainList *cta_train_list;
} CtaTrainListTransaction;

typedef struct CtaFavoritesTransaction {
    FavoritesList *favorites_list;
} CtaFavoritesTransaction;

union CtaTransactionData {
    CtaTrainListTransaction train_list_transaction;
    CtaFavoritesTransaction favorites_transaction;
};

typedef struct CtaCommTransaction {
    enum CtaTransactionType transaction_type;
    bool    transaction_done;
    CtaCommPerformTransactionCallback callback;
    union CtaTransactionData transaction_data;
    int transaction_index;
} CtaCommTransaction;

void comm_init(void);
bool comm_perform_transaction(CtaCommTransaction *transaction);

#endif /* COMM_H */
