#ifndef FAVORITES_H
#define FAVORITES_H

#include "lib/color_menu_layer.h"

#define FAVORITES_NAME_SIZE (51)

typedef struct Favorite {
    char name[FAVORITES_NAME_SIZE];
    uint32_t station_id;
} Favorite;

typedef struct FavoritesList {
    int favorites_count;
    Favorite *favorites;
} FavoritesList;

FavoritesList* favorites_list_create(int16_t favorites_count);
void favorites_list_destroy(FavoritesList *favorites_list);

#endif /* FAVORITES_H */
