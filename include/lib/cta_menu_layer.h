#ifndef CTA_MENU_LAYER_H
#define CTA_MENU_LAYER_H

#include <pebble.h>
#include "cta_lines.h"

typedef struct CtaMenuLayer {
    MenuLayer    *menu_layer;
    CtaTrainList *train_list;
} CtaMenuLayer;

CtaMenuLayer *cta_menu_layer_create(Window *window, CtaTrainList *train_list);
void cta_menu_layer_destroy(CtaMenuLayer *cta_menu_layer);

#endif /* CTA_MENU_LAYER_H */
