Pebble CTA - Track Chicago 'L' arrival times from your Pebble smartwatch!
=========================================================================

.. image:: sample.gif
           :align: center

Pebble CTA needs to be built using the `Pebble SDK`_.

.. _`Pebble SDK`: https://developer.rebble.io/developer.pebble.com/sdk/index.html
